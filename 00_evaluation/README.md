# Leistungsbeurteilung Modul 231 (V1)

Die Schlussnote setzt sich wie folgt zusammen:
```
N = LB1*0.3 + LB2*0.7
```

# LB1 - Schriftliche Prüfung

|   |   |
|---|---|
| Dauer  | 1 Lektion  |
| Erlaubte Hilfsmittel | Selbstgeschriebene Zusammenfassung (siehe Hinweise) |
| Zulassungsbedingung | Selbstgeschriebene Zusammenfassung |
| Nicht erfüllen der Zulassungsbedingungen | 1 Note Abzug |
| Elektronische Hilfsmittel | Nicht erlaubt |

Der Prüfungsinhalt umfasst alle bis dahin besprochenen
 Unterlagen. Der definitive Prüfungsinhalt wird zwei Wochen vorher bekanntgegeben. 

## Inhalt
 - Grundlagen Datenschutz (Lehrmittel EDÖB)
 - Schutzwürdigkeit von Daten im Internet
   - A1G: Kann schützenswerte Daten erkennen.
   - A1E: Kann unterschiedliche Regelungen (zBps DSG, DSGVO, Schulordnung) vergleichen und kritisch hinterfragen.
 - Git
   - Kennt die Grundlagen von Git
   - Kennt die wichtigsten Befehle (git add, git push, git pull, git commit, git status, git clone, git checkout)
 - Passwörter
   - E1G: Kann die Prinzipien der Passwortverwaltung und "gute" Passwörter erläutern.
   - Kennt den Unterschied zwischen Authentifizierung und Autorisierung
   - Kennt Methoden der Mehrfaktor-Authentifizierung

<br>Unterlagen: *Präsentationen*, *GitLab Repository zum Modul*, *Unterlagen auf Teams*

## Persönliche Zusammenfassung
 - Umfang:
   - Ausgedruckt: Max. 2 x A4 Papier (=4 Seiten)
   - Handgeschrieben: Max 4 x A4 Papier (=8 Seiten)
 - Individuelle und selbstgeschriebene Zusammenfassung
 - Keine „Collagen“: Aus den Unterlagen 1:1 übernommenen Text oder
Seitenausschnitte sind nicht zugelassen.
   - Ausschnitte von einzelnen Grafiken / Schemas ist erlaubt.
 - Mindestumfang: Wichtigste Punkte jedes prüfungsrelevanten Themas
 - Zusammenfassung muss mit der Prüfung abgegeben werden!

# LB2 - Persönliche Dossier (V2, 14.11.2021)

 - K3, K4, K6, K7 müssen bis zu einem bestimmten Tag der Lehrperson gezeigt werden.
 - Alle weiteren Kriterien werden am finalen Besprechungstermin bewertet.
 - **Informieren Sie sich über die Abgabetermine bei der Lehrperson!**



| N° | Kriterien                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | Max  |
|----|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------|
| 1  | Zum Besprechungstermin pünktlich erschienen (1P) und diesen erfolgreich durchgeführt (Repository ist fertig und kann präsentiert werden (1P), Applikationen auf dem Laptop geöffnet und bereit zum Präsentieren (1P), Die Aussagen des Lernende(r) sind mehrheitlich korrekt und sachbezogen (1P) ).                                                                                                                                                                                                                | 4 P  |
| 2  | Persönliche Tages- und Wochenziele und jeweilige Auswertung (Welche neuen Inhalte habe ich heute gelernt? Was habe ich erreicht? Wo hatte ich Schwierigkeiten? Wo muss ich mich noch verbessern?) fortlaufend im Lernportfolio festgehalten. (1 P pro Tag (maximal 8))                                                                                                                                                                                                                                             | 8 P  |
| 3  | Github oder Gitlab Account angelegt (1P), Repository für eigenes Lernportfolio angelegt (1P), Name des Repository bezieht sich auf den Inhalt und ist nachvollziehbar (1P), Repository der Lehrperson freigeben (1P)<br/> (Abgabedatum siehe Einstiegspräsentation (PDF))                                                                                                                                                                                                                                          | 4 P  |
| 4  | Das persönliche Repository enthält ein README.md (1P), sinnvolle Struktur gewählt (Unterseiten) (1P), Navigation vorhanden (1P), alle Unterseiten verlinkt (1P), Navigation ist intuitiv (1P)<br/>(Abgabedatum siehe Einstiegspräsentation (PDF))                                                                                                                                                                                                                                                                  | 5 P  |
| 5  | Individuelles Programm (Desktop, Mobiltelefon-App oder Webapplikation/Service) hinsichtlich des Datenschutzes analysiert und wichtigste Punkte (Welche personenbezogenen Daten werden gespeichert? (Liste, 1P) Wo werden die Daten gespeichert?, Zu welchem Zweck werden meine Daten verwendet? (1P), Werden die Daten an Dritte weitergegeben und zu welchem Zweck? (1P), Welche Metriken werden wie häufig und zu welchem Zweck erfasst (1P), usw.). <br/>Analysiertes Programm muss mit Lehrperson abgesprochen sein! | 4 P  |
| 6  | Passwortverwaltungsprogramm ausgewählt (1P), Entscheid begründet (Vergleich mit anderer Software, Vor und Nachteile) (1P)<br/>(Abgabedatum siehe Einstiegspräsentation (PDF))                                                                                                                                                                                                                                                                                                                                      | 2 P  |
| 7  | Passwortverwaltungsprogramm auf eigenem Gerät installiert und der Lehrperson gezeigt. (Abgabedatum siehe Einstiegspräsentation (PDF))                                                                                                                                                                                                                                                                                                                                                                              | 4 P  |
| 8  | Ablagekonzept entwickelt: Unterschiedliche Daten (Eigene Daten auf dem Persönlichen- oder Geschäftslaptop) nach Kriterien (Schutzwürdigkeit) eingeteilt (1P), Unterschiedliche Speicherorte analysiert und Datenschutz beurteilt (1P), (Ablage- /Ordner-)Struktur entwickelt und dokumentiert.                                                                                                                                                                                                                     | 2 P  |
| 9  | Backupstrategie definiert (2P) und umgesetzt (2P)                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | 4 P  |
| 10 | Zwei Checklisten (pro Checkliste 1P) des Datenschutzbeauftragten des Kanton Zürichs bearbeitet und gemäss Aufgabenbeschreibung dokumentiert. | 2 P  |
| 11 | Quellen zu Entscheidungsgrundlagen und Links (z.B. zu Programmen) sind immer vorhanden und können ohne Suchen dem Inhalt zugeordnet werden (3 P für Vollständig, 2 P für Mehrheitlich, 1P für teilweise, 0 P für nicht)                                                                                                                                                                                                                                                                                            | 3 P  |
| 12 | Zusatzpunkte kann es zum Beispiel geben für:<br/>Kritische Hinterfragung mit fachlicher Differenzierung von Datenschutzgesetzen/Datenschutzbestimmungen; Umfangreiches Ablagekonzept, welches alle Daten umfasst, die vom Lernenden bearbeitet werden; Mehrstufiges Backupkonzept, welches gegen alle Risiken bestmöglich standhält; Sehr gute Übersichtlichkeit und Darstellung; Kein Ballast; usw.                                                                                                                  | 11 P |
|    | **Total**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | **53 P** |
