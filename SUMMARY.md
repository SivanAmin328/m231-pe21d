# Summary

## Themen
 - [Einführung](/01_protection/README.md)
 - [GIT](/02_git/README.md)
 - [Passwords](/03_passwords/README.md)

## Administratives
 - [Kompetenzmatrix](00_kompetenzband/README.md)
 - [Leistungsbeurteilung](/00_evaluation/README.md)